import { Component, ViewChild, OnInit } from '@angular/core';
import { POSTOS } from '../listaPostos';

declare var google;

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})

export class Tab1Page implements OnInit {
  map;
  @ViewChild('mapElement') mapElement;
  postos = POSTOS;

  lat_inicial: number = -15.7801
  lng_inicial: number = -47.9292
  
  i: number
  j: number
  x;
  min = 0

  ngOnInit (){
    this.postos.sort((a, b) => a.preco_gasolina < b.preco_gasolina ? -1 : a.preco_gasolina > b.preco_gasolina ? 1 : 0);
  }


  ngAfterContentInit():void{
    this.map = new google.maps.Map(this.mapElement.nativeElement,
      {
        center: {lat: this.lat_inicial, lng: this.lng_inicial}
      }
      )
  }
}